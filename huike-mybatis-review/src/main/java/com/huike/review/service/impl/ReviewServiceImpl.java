package com.huike.review.service.impl;

import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.domain.entity.SysUser;
import com.huike.common.core.page.TableDataInfo;
import com.huike.common.exception.CustomException;
import com.huike.common.utils.bean.BeanUtils;
import com.huike.review.pojo.Review;
import com.huike.review.service.ReviewService;
import com.huike.review.mapper.MybatisReviewMapper;
import com.huike.review.vo.MybatisReviewVO;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * mybatis复习使用的业务层
 * 注意该业务层和我们系统的业务无关，主要是让同学们快速熟悉系统的
 */
@Service
public class ReviewServiceImpl implements ReviewService {

    @Autowired
    private MybatisReviewMapper reviewMapper;
    @Autowired
    private SqlSessionFactory sqlSessionFactory;


    /**
     * =========================================================保存数据的方法=============================================
     */
    @Override
    public boolean save(String name, Integer age, String sex) {
        MybatisReviewVO mybatisReviewVO = new MybatisReviewVO();
        mybatisReviewVO.setName(name);
        mybatisReviewVO.setAge(age);
        mybatisReviewVO.setSex(sex);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        MybatisReviewMapper mapper = sqlSession.getMapper(MybatisReviewMapper.class);
        int i = mapper.save(mybatisReviewVO);
        if (i>0){
            return true;
        }
        return false;
    }

    @Override
    public boolean save(MybatisReviewVO mybatisReviewVO) {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        MybatisReviewMapper mapper = sqlSession.getMapper(MybatisReviewMapper.class);
        int i = mapper.save(mybatisReviewVO);
        if (i>0){
            return true;
        }
        return false;
    }


    /**=========================================================删除数据=============================================*/

    @Override
    public boolean deleteById(Integer id) {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        MybatisReviewMapper mapper = sqlSession.getMapper(MybatisReviewMapper.class);
        int i = mapper.deleteById(id);
        if (i>0){
            return true;
        }
        return false;
    }


    /**
     * =========================================================修改数据=============================================
     */
    @Override
    public boolean update(MybatisReviewVO mybatisReviewVO) {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        MybatisReviewMapper mapper = sqlSession.getMapper(MybatisReviewMapper.class);
        int i = mapper.update(mybatisReviewVO);
        if (i>0){
            return true;
        }
        return false;
    }



    /**=========================================================查询数据的方法=============================================*/

    @Override
    public SysUser getById(Integer id) {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        MybatisReviewMapper mapper = sqlSession.getMapper(MybatisReviewMapper.class);
        SysUser sysUser = mapper.getById(id);
        return sysUser;
    }

    @Override
    public TableDataInfo getDataByPage(Integer pageNum, Integer pageSiza) {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        MybatisReviewMapper mapper = sqlSession.getMapper(MybatisReviewMapper.class);
        TableDataInfo tableDataInfo = mapper.getDataByPage(pageNum, pageSiza);
        return tableDataInfo;
    }

}
