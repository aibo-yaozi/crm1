package com.huike.review.service;

import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.domain.entity.SysUser;
import com.huike.common.core.page.TableDataInfo;
import com.huike.review.pojo.Review;
import com.huike.review.vo.MybatisReviewVO;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * mybatis复习接口层
 */
public interface ReviewService {


    boolean save(String name, Integer age, String sex);

    boolean save(MybatisReviewVO mybatisReviewVO);

    boolean update(MybatisReviewVO mybatisReviewVO);

    boolean deleteById(Integer id);

    SysUser getById(Integer id);

    TableDataInfo getDataByPage(Integer pageNum,Integer pageSiza);
}
