package com.huike.review.mapper;

import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.domain.entity.SysUser;
import com.huike.common.core.page.TableDataInfo;
import com.huike.review.pojo.Review;
import com.huike.review.vo.MybatisReviewVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Mybatis复习的Mapper层
 */
public interface MybatisReviewMapper {


    /**======================================================新增======================================================**/
    int save(MybatisReviewVO mybatisReviewVO);





    /**======================================================删除======================================================**/
    int deleteById(Integer id);
    /**======================================================修改======================================================**/
    int update(MybatisReviewVO mybatisReviewVO);



    /**======================================================简单查询===================================================**/
    SysUser getById(Integer id);

    TableDataInfo getDataByPage(@Param("pageNum")Integer pageNum,@Param("pageSiz") Integer pageSiza);
}
