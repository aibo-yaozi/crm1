package com.huike.clues.service;


import com.huike.clues.domain.TbClue;
import com.huike.clues.domain.TbClueTrackRecord;
import com.huike.clues.domain.vo.ClueTrackRecordVo;

import java.util.List;

/**
 * 线索跟进记录Service接口
 * 
 * @author WGL
 * @date 2022-04-19
 */
public interface ITbClueTrackRecordService {
    /**
     *查询线索跟进记录列表
     * @param clueId
     * @return
     */

    List<TbClueTrackRecord> getById(Long clueId);


    /**
     * 新增跟进记录
     * @param tbClue
     * @param tbClueTrackRecord
     * @return
     */
    int insertTbclueTrackRecord(TbClue tbClue, TbClueTrackRecord tbClueTrackRecord);
}
