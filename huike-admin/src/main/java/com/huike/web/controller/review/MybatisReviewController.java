package com.huike.web.controller.review;


import com.huike.common.constant.HttpStatus;
import com.huike.common.core.controller.BaseController;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.domain.entity.SysUser;
import com.huike.common.core.page.TableDataInfo;
import com.huike.review.service.ReviewService;
import com.huike.review.vo.MybatisReviewVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 该Controller主要是为了复习三层架构以及Mybatis使用的，该部分接口已经放开权限，可以直接访问
 * 同学们在此处编写接口通过浏览器访问看是否能完成最简单的增删改查
 */
@RestController
@RequestMapping("/review")
public class MybatisReviewController extends BaseController {

    @Autowired
    private ReviewService reviewService;

    /**
     * =========================================================新增数据============================================
     */
    @GetMapping("/saveData/{name}/{age}/{sex}")
    public AjaxResult saveData(@PathVariable String name, @PathVariable Integer age, @PathVariable String sex) {
        boolean flag = reviewService.save(name, age, sex);
        if (flag) {
            return AjaxResult.success("成功插入：1条数据", HttpStatus.SUCCESS);
        }
        return AjaxResult.error("插入失败",HttpStatus.ERROR);

    }

    @PostMapping("/saveData")
    public AjaxResult saveData(MybatisReviewVO mybatisReviewVO) {
        boolean flag = reviewService.save(mybatisReviewVO);
        if (flag) {
            return AjaxResult.success("成功插入：1条数据", HttpStatus.SUCCESS);
        }
        return AjaxResult.error("插入失败",HttpStatus.ERROR);
    }

    /**
     * =========================================================删除数据=============================================
     */
    @DeleteMapping("/remove/{id}")
    public AjaxResult remove(@PathVariable Integer id) {
        boolean flag = reviewService.deleteById(id);
        if (flag) {
            return AjaxResult.success("操作成功", HttpStatus.SUCCESS);
        }
        return AjaxResult.error("操作失败",HttpStatus.ERROR);

    }

    /**
     * =========================================================修改数据=============================================
     */
    @PostMapping("/update")
    public AjaxResult update(MybatisReviewVO mybatisReviewVO) {
        boolean flag = reviewService.update(mybatisReviewVO);
        if (flag) {
            return AjaxResult.success("修改成功", HttpStatus.SUCCESS);
        }
        return AjaxResult.error("修改失败",HttpStatus.ERROR);

    }

    /**
     * =========================================================查询数据=============================================
     */
    @GetMapping("/getById")
    public AjaxResult getById(@RequestBody Integer id) {
        SysUser sysUser = reviewService.getById(id);
        return AjaxResult.success("操作成功", sysUser);
    }

    @GetMapping("/getDataByPage")
    public TableDataInfo getDataPage(@RequestBody Integer pageNum, @RequestBody Integer pageSize) {
        TableDataInfo tableDataInfo = reviewService.getDataByPage(pageNum, pageSize);
        tableDataInfo.setCode(HttpStatus.SUCCESS);
        tableDataInfo.setMsg("查询成功");
        tableDataInfo.setTotal(tableDataInfo.getRows().size());
        return tableDataInfo;
    }
}
